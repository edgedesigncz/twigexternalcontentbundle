<?php

namespace Edge\TwigExternalContentBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle class for TwigExternalContentBundle
 */
class TwigExternalContentBundle extends Bundle
{
}
