<?php


namespace Edge\TwigExternalContentBundle\Twig;

use Edge\TwigExternalContentBundle\Requester\RequesterInterface;
use Exception;
use Twig_Extension;
use Twig_Function_Method;


/**
 * Twig Extension class for registering bundle macros.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class LoadContentExtension extends Twig_Extension
{
    const CONTENT_CANNOT_BE_SERVED = 'E_CONTENT_CANNOT_BE_SERVED';

    /** @var \Edge\TwigExternalContentBundle\Requester\RequesterInterface */
    private $requester;

    public function __construct(RequesterInterface $requester)
    {
        $this->requester = $requester;
    }

    public function getFunctions()
    {
        return array(
            'loadExternalContent' => new Twig_Function_Method($this, 'loadExternalContent', array('is_safe' => array('html')))
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'edge_twig_external_content';
    }

    /**
     * This method tries to load external content from given $url.
     *
     * If, for some reason, content cannot be returned (i.e. requested served does not responds, or velociraptor ate the router)
     * the methods returns string in self::CONTENT_CANNOT_BE_SERVED trailed with message of the exception closed in brackets.
     * Exception cannot be thrown, because twig cannot handle them.
     *
     * @param string $url
     * @return string
     */
    public function loadExternalContent($url)
    {
        try {
            return $this->requester->get($url);
        } catch (Exception $e) {
            return sprintf('%s (%s)', self::CONTENT_CANNOT_BE_SERVED, $e->getMessage());
        }
    }
}