TwigExternalContentBundle
=========================

This is a simple bundle, that adds new Twig macro called loadExternalContent.

Usage
-----
```twig 
{{ loadExternalContent('http://google.com') }}
```

It is highly encouraged to apply some sanitization filter. The choice of right filter is left up to author of the template.

Caching
-------
Caching is done on the background by Guzzle. It tries to negotiate optimum cache-control with HTTP server. 
It is also possible to set default TTL in Resources/config/config.yml.