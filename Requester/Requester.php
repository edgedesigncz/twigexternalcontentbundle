<?php


namespace Edge\TwigExternalContentBundle\Requester;


use Guzzle\Http\Client;
use Guzzle\Plugin\Cache\CachePlugin;

/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class Requester implements RequesterInterface
{

    /** @var CachePlugin */
    private $cachePlugin;

    /**
     * If CachePlugin is null, no caching is done.
     *
     * @param CachePlugin|null $cachePlugin
     */
    public function __construct(CachePlugin $cachePlugin = null)
    {
        $this->cachePlugin = $cachePlugin;
    }

    /**
     * Get content on given URL
     *
     * @param $url
     * @return mixed
     */
    public function get($url)
    {
        $client = $this->getClient($url);

        $request = $client->get();

        $response = $request->send();

        return $response->getBody(true);
    }

    /**
     * Get configured Client instance for given $url with set-up cache, ttl.
     *
     * @param $url
     * @return Client
     */
    public function getClient($url)
    {
        $client = new Client($url);

        if ($this->cachePlugin) {
            $client->addSubscriber($this->cachePlugin);
        }

        return $client;
    }
}