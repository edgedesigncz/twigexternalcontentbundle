<?php


namespace Edge\TwigExternalContentBundle\Requester;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
interface RequesterInterface 
{
    /**
     * Get content on given URL
     *
     * @param $url
     * @return mixed
     */
    public function get($url);
}